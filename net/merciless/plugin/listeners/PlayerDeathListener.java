package net.merciless.plugin.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import net.merciless.plugin.Main;
import net.merciless.plugin.utils.Utils;

public class PlayerDeathListener implements Listener {

	private static Main plugin;
	
	@SuppressWarnings("static-access")
	public PlayerDeathListener(Main plugin) {
		this.plugin = plugin;

		Bukkit.getPluginManager().registerEvents(this, plugin);
	}


	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if(e.getEntity().getKiller() instanceof Player){
			
			Player killer = e.getEntity().getKiller();
			Player p = e.getEntity();
			Bukkit.broadcastMessage(Utils.chat("&d&lThe Nebula &8| &5" + p.getDisplayName() + "&f has been killed- by &5" + killer.getDisplayName()));
		}
	}
	
}
