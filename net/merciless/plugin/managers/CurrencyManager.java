package net.merciless.plugin.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import net.merciless.plugin.Main;

public class CurrencyManager {

	public static HashMap<String, Integer> currency = new HashMap<String, Integer>();
	
	public Main plugin;
	public CurrencyManager(Main plugin) {
		this.plugin = plugin;
	}
	
	public void saveCurrencyFile() throws FileNotFoundException, IOException {
		for (OfflinePlayer p : Bukkit.getOfflinePlayers()) {
			File file = new File("NebulaCore/currency.dat");	
			ObjectOutputStream output = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(file)));
		
			@SuppressWarnings("unused")
			UUID uuid = p.getUniqueId();
			
			try {
				output.writeObject(currency);
				output.flush();
				output.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadCurrencyFile() throws FileNotFoundException, IOException, ClassNotFoundException {
		File file = new File("NebulaCore/currency.dat");	
		if( file != null) {
			ObjectInputStream input = new ObjectInputStream(new GZIPInputStream(new FileInputStream(file)));
			Object readObject = input.readObject();
			input.close();
			
			if (!(readObject instanceof HashMap)) {
				throw new IOException("Data is not a hashmap");
			}
			currency = (HashMap<String, Integer>) readObject;
			for (String key : currency.keySet()) {
				currency.put(key,  currency.get(key));
			}
		}
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public void addCurrencyToPlayer(OfflinePlayer p, int amount) {
		UUID uuid = p.getUniqueId();
		if(currency.get(p.getUniqueId()) != null) {
			currency.put(uuid.toString(), currency.get(p.getUniqueId()) + amount);
		} else {
			currency.put(uuid.toString(), amount);
		}
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public void removeCurrencyFromPlayer(OfflinePlayer p, int amount) {
		UUID uuid = p.getUniqueId();
		if(currency.get(p.getUniqueId()) != null) {
			currency.put(uuid.toString(), currency.get(p.getUniqueId()) - amount);
		}
	}
	
	public void setPlayerCurrency(OfflinePlayer p, int amount) {
		currency.put(p.getUniqueId().toString(), amount);
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public int getPlayerCurrency(OfflinePlayer p) {
		if(currency.get(p.getUniqueId()) != null) {
			return currency.get(p.getUniqueId());
		} else {
			return 0;
		}
	}
}
