package net.merciless.plugin;

import java.io.IOException;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import net.merciless.plugin.commands.CurrencyCommand;
import net.merciless.plugin.commands.FlightCommand;
import net.merciless.plugin.commands.HelloCommand;
import net.merciless.plugin.commands.BanCommand;
import net.merciless.plugin.listeners.InventoryClickListener;
import net.merciless.plugin.listeners.JoinListener;
import net.merciless.plugin.listeners.PlayerDeathListener;
import net.merciless.plugin.managers.CurrencyManager;
import net.merciless.plugin.ui.TestUI;
import net.merciless.plugin.utils.Utils;

public class Main extends JavaPlugin {

	@Override
	public void onEnable() {
		startup();
		registerManagers();
		CurrencyManager currencyManager = new CurrencyManager(this);
		try {
			currencyManager.loadCurrencyFile();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		registerListeners();
		registerCommands();
		registerUIS();
		saveDefaultConfig();
	}
	
	@Override
	public void onDisable() {
		shutdown();
		CurrencyManager currencyManager = new CurrencyManager(this);
		try {
			currencyManager.saveCurrencyFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void shutdown() {
		ConsoleCommandSender console = getServer().getConsoleSender();
		console.sendMessage(Utils.chat("&5======================================================="));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
 		console.sendMessage(Utils.chat("&6__  __               _ _                  _____"));               
 		console.sendMessage(Utils.chat("&6|  \/  |             (_) |                / ____|   "));           
 		console.sendMessage(Utils.chat("&6| \  / | ___ _ __ ___ _| | ___  ___ ___  | |     ___  _ __ ___ "));
 		console.sendMessage(Utils.chat("&6| |\/| |/ _ \ '__/ __| | |/ _ \/ __/ __| | |    / _ \| '__/ _ "));
 		console.sendMessage(Utils.chat("&6| |  | |  __/ | | (__| | |  __/\__ \__ \ | |___| (_) | | |  __/"));
 		console.sendMessage(Utils.chat("&6|_|  |_|\___|_|  \___|_|_|\___||___/___/  \_____\___/|_|  \___|"));
                                                                
                                                                

		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6Merciless Core, now DISABLED"));
		console.sendMessage(Utils.chat("&6Good Bye! "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&5======================================================="));	
	}
	
	public void startup() {
		ConsoleCommandSender console = getServer().getConsoleSender();
		console.sendMessage(Utils.chat("&5======================================================="));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6__  __               _ _                  _____"));               
 		console.sendMessage(Utils.chat("&6|  \/  |             (_) |                / ____|   "));           
 		console.sendMessage(Utils.chat("&6| \  / | ___ _ __ ___ _| | ___  ___ ___  | |     ___  _ __ ___ "));
 		console.sendMessage(Utils.chat("&6| |\/| |/ _ \ '__/ __| | |/ _ \/ __/ __| | |    / _ \| '__/ _ "));
 		console.sendMessage(Utils.chat("&6| |  | |  __/ | | (__| | |  __/\__ \__ \ | |___| (_) | | |  __/"));
 		console.sendMessage(Utils.chat("&6|_|  |_|\___|_|  \___|_|_|\___||___/___/  \_____\___/|_|  \___|"));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6Merciless Core, now enabled"));
		console.sendMessage(Utils.chat("&6version: 0.1"));
		console.sendMessage(Utils.chat("&6Developed by: Gman65"));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&5======================================================="));
	}
	
	public void registerManagers() {
		new CurrencyManager(this);
	}
	
	public void registerUIS() {
		TestUI.initialize();
	}
	
	public void registerCommands() {
		new FlightCommand(this);
		new HelloCommand(this);
		new CurrencyCommand(this);
	}
	
	public void registerListeners() {
		new JoinListener(this);
		new InventoryClickListener(this);
		new PlayerDeathListener(this);
	}
         getCommand("bangui").setExecutor(new BanCommand());
}
