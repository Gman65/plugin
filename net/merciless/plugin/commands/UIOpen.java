package net.merciless.plugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.merciless.plugin.Main;
import net.merciless.plugin.ui.TestUI;

public class UIOpen implements CommandExecutor {
	
	@SuppressWarnings("unused")
	private Main plugin;
	
	public UIOpen(Main plugin) {
		this.plugin = plugin;
		
		plugin.getCommand("uiopen").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lavel, String[] args) {

		if(!(sender instanceof Player)) {
			//send console a message sender.sendMessage();
			return true;
		}
		
		Player p = (Player) sender;
		
		if(p.hasPermission("tn.ui")) {
			p.openInventory(TestUI.GUI(p));
		}
		
		return false;
	}
	
	
	
}
