package me.gmanstest.bangui.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class BanCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){


        if (sender instanceof Player) {
            Player player = (Player) sender;
            ArrayList<Player> player_list = new ArrayList<>(player.getServer().getOnlinePlayers());

            Inventory bangui = Bukkit.createInventory(player, 45, ChatColor.BLUE + "Player List!");
            //For every player, add their name and head to the gui
            for (int i = 0; i < player_list.size();i++)
            {
                ItemStack playerhead = new ItemStack(Material.CAKE, 1);
                ItemMeta meta = playerhead.getItemMeta();
                meta.setDisplayName((player_list.get(i).getDisplayName()));
                ArrayList<String> lore = new ArrayList<>();
                lore.add(ChatColor.GOLD + "Player Health: " + player_list.get(i).getHealth());
                lore.add(ChatColor.GOLD + "XP: " + player_list.get(i).getExp());
                meta.setLore(lore);
                playerhead.setItemMeta(meta);

                        bangui.addItem(playerhead);
            }

            player.openInventory(bangui);
        }








        return true;

    }

}
