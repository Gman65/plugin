package net.merciless.plugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.merciless.plugin.Main;
import net.merciless.plugin.utils.Utils;

public class FlightCommand implements CommandExecutor{

	private Main plugin;
	
	public FlightCommand(Main plugin) {
		this.plugin = plugin;
		
		plugin.getCommand("fly").setExecutor(this);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.chat(plugin.getConfig().getString("console_error_message")));
			return true;
		}
		
		Player p = (Player) sender;
		
		if(p.hasPermission("tn.fly")) {
			if (p.isFlying()) {
				p.setAllowFlight(false);
				p.setFlying(false);
				p.sendMessage(Utils.chat(plugin.getConfig().getString("message_prefix" + "FlyCommand.flying_disable")));
			} else {
				p.setAllowFlight(true);
				p.setFlying(true);
				p.sendMessage(Utils.chat(plugin.getConfig().getString("message_prefix" + "FlyCommand.flying_enable")));
			}
		} else {
			p.sendMessage(Utils.chat(plugin.getConfig().getString("message_prefix" + "no_perm_message")));
		}
		
		return false;
	}
	
}
