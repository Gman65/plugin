package net.merciless.plugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import net.merciless.plugin.Main;
import net.merciless.plugin.managers.CurrencyManager;
import net.merciless.plugin.utils.Utils;

public class CurrencyCommand implements CommandExecutor {

	public Main plugin;
	
	public CurrencyCommand(Main plugin) {
		this.plugin = plugin;

		plugin.getCommand("currency").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender.hasPermission("tn.currency.use")) {
			if (args.length == 0) {
				sender.sendMessage(Utils.chat("&d&lMerciless &8| &f/currency <add:remove:set:get> <player> (amount)"));
				return true;
			} else if (args.length == 1) {
				
				if(args[0].equalsIgnoreCase("add")) {
					sender.sendMessage(Utils.chat("&d&lMerciless &8| &f/currency add <player> <amount>"));
				} else if(args[0].equalsIgnoreCase("remove")) {
					sender.sendMessage(Utils.chat("&d&lMerciless &8| &f/currency remove <player> <amount>"));
				} else if(args[0].equalsIgnoreCase("set")) {
					sender.sendMessage(Utils.chat("&d&lMerciless &8| &f/currency set <player> <amount>"));
				} else if(args[0].equalsIgnoreCase("get")) {
					sender.sendMessage(Utils.chat("&d&lMerciless &8| &f/currency get <player>"));
				}
				
			} else if (args.length == 2) {
				CurrencyManager manager = new CurrencyManager(plugin);
				OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);
				
				
				if (p != null) {
					if(args[0].equalsIgnoreCase("get")) {
						sender.sendMessage(Utils.chat("&d&lMerciless &8| &5" + p.getName() + " &fcurrently has a balance of " + "&5�" + manager.getPlayerCurrency(p)));
					    return true;
					} else {
						sender.sendMessage(Utils.chat("&d&lMerciless &8|  &f/currency " + args[0] + p.getName() + " <amount>"));
					}
				} else {
					sender.sendMessage(Utils.chat("&d&lMerciless &8| &fPlayer &5 " + args[1] + " &f could not be found"));
				}
			} else if (args.length == 3) {
				OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);
				int amount = Integer.parseInt(args[2]);
				CurrencyManager manager = new CurrencyManager(plugin);
				if(args[0].equalsIgnoreCase("add")) {
					if (p != null) {
						manager.addCurrencyToPlayer(p, amount);
						sender.sendMessage(Utils.chat("&d&lMerciless &8|  &fYou have sucessfully added " + "&5�" + args[2] + " &fto " + p.getName()));
					} else {
						sender.sendMessage(Utils.chat("&d&lMerciless &8| &fPlayer &5 " + args[1] + " &fcould not be found"));
					}
				} else if(args[0].equalsIgnoreCase("remove")) {
					if (p != null) {
						manager.removeCurrencyFromPlayer(p, amount);
						sender.sendMessage(Utils.chat("&d&lMerciless &8| &fYou have sucessfully removed " + "&5�" + args[2] + " &ffrom " + p.getName()));
					} else {
						sender.sendMessage(Utils.chat("&d&lMerciless &8| &fPlayer &5 " + args[1] + " &f could not be found"));
					}
				} else if(args[0].equalsIgnoreCase("set")) {
					if (p != null) {
						manager.setPlayerCurrency(p, amount);
						sender.sendMessage(Utils.chat("&d&lMerciless &8| &fYou have sucessfully set " + p.getName() + "&f`s balance to " + "&5�" + args[2]));
					} else {
						sender.sendMessage(Utils.chat("&d&lMerciless &8| &fPlayer &5 " + args[1] + " &f could not be found"));
					}
				}
			}
		} else {
			sender.sendMessage(Utils.chat(plugin.getConfig().getString("&d&lMerciless &8| " + "no_perm_message")));
		}
		
		return false;
	}

	
		
	
}
